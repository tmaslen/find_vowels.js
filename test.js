'use strict'

const expect = require( "chai" ).expect;

const findVowels = require( "./index" );

describe( "find vowels", () => {

    it( "returns an array of vowels found in parameter", () => {

        expect( findVowels( "aaa" ) ).to.deep.equal( [ "a", "a", "a" ] );
        expect( findVowels( "abcdef" ) ).to.deep.equal( [ "a", "e" ] );
        expect( findVowels( "bbb" ) ).to.deep.equal( [] );

    });

});